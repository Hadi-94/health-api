var request = require("supertest");
// var app = require("../app.js");

describe("GET /", () => {
  var server;

  beforeEach(() => {
    server = require("../app.js").listen(3001);
  });

  afterEach((done) => {
    server.close(done);
  });

  after((done) => {
    process.exit(0);
  });

  it("respond with server is healthy", (done) => {
    request(server).get("/").expect(200);
    done();
  });
});
